% after designing pulses using run_rootflipmbref.m, call this script
% to write it out for the 8-channel multix system using the Utrecht format
% 2015, Will Grissom, Vanderbilt University

rfg = load('rootflibmbresults.mat');
gmax = 2; % g/cm, max grad amp for crushers
gslew = 14000; % g/cm/s, max grad slew for crushers and ramps
dt = 6.4e-6; % RF + grad dwell time for scanner
crusharea = 0.001*gmax; % 1 ms at max grad amp
te = 20e-3; % desired TE in ms
examcardte = 2.1e-3; % time from the end of the pulse to the middle of the acq window. 
% With the Utrecht patch, this is the TE requested on the examcard

% interpolate 90+180 to scanner dwell time 
rf90i = interp1(rfg.t90,rfg.rf90,(-rfg.T90/2:dt:rfg.T90/2-dt)*1000,'spline',0);
rf180i = interp1(rfg.t180,rfg.rf180,(-rfg.T180/2:dt:rfg.T180/2-dt)*1000,'spline',0);
rf90i = rf90i*sum(abs(rfg.rf90))/sum(abs(rf90i)); % force same area
rf180i = rf180i*sum(abs(rfg.rf180))/sum(abs(rf180i)); % force same area
rf90i = rf90i/(2*pi*dt*4258); % convert to Gauss
rf180i = rf180i/(2*pi*dt*4258); % convert to Gauss
        
% add ramps to the 90 + 180 gradient waveforms
nramp = ceil(rfg.g90/gslew/dt); % number of points on the 90 ramps
g90 = rfg.g90*[(0:nramp-1)'/nramp;ones(length(rf90i),1);(nramp-1:-1:0)'/nramp];
rf90i = [zeros(nramp,1);rf90i(:);zeros(nramp,1)];
nramp = ceil(rfg.g180/gslew/dt); % number of points on the 180 ramps
g180 = rfg.g180*[(0:nramp-1)'/nramp;ones(length(rf180i),1);(nramp-1:-1:0)'/nramp];
rf180i = [zeros(nramp,1);rf180i(:);zeros(nramp,1)];

% design crushers
crusher = dotrap(crusharea,gmax,gslew,dt);

% calculate minTE, check that requested TE is long enough
minTE = dt*(length(rf90i)+2*length(crusher)+length(rf180i));
if te < minTE
    error('Requested TE is too long; min TE is %0.2f ms',minTE);
end

% calculate zero padding needed between 90 and the first crusher
nzeros = ceil((te-minTE)/(2*dt));

% stitch together the RF and gradient waveforms; add zeros for
% requested TE
rf = [rf90i(:);zeros(nzeros,1);0*crusher(:);rf180i(:);0*crusher(:)];
g = [g90(:);zeros(nzeros,1);crusher(:);g180(:);crusher(:)];

% check that we still get nice profiles with final interpolated pulses

% write them to file

% tell the user what to set ExamCard TE to, since it will be referenced
% to the end of the pulses
