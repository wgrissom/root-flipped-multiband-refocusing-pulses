% Script to design a root-flipped multiband refocusing pulse and
% a matched-phase excitation pulse.
% 
% Implements the algorithm in:
%   A Sharma, M Lustig, and W A Grissom. Root-flipped multiband refocusing
%       pulses. Magn Reson Med 2015. doi: 10.1002/mrm.25629
%
% 2015 Anuj Sharma and Will Grissom, Vanderbilt University

gamma = 2*pi*4258; % 1H gyromagnetic ratio (radians/second/gauss)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = 512;                % # of time points
nb = 6;                 % # of bands
tb = 4;                 % Time-bandwidth product
slthick = 3;            % mm, slice thickness
bandsep = 4*tb;         % Slice gap is 4 slice thickness
alignedecho = 0;        % 0 = design a minimum-duration 90
d1e = 0.01;             % combined Mxy ripple, passband
d2e = 0.01;             % combined Mxy ripple, stopband
maxb1 = 0.13;           % Gauss, system b1 limit
rftype = 'matched';     % 'matched' or '2xrefd' (twice-refocused)
filtAlg = 'cvx';        % 'cvx' or 'firls' filter design algorithm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the refocusing pulse design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[rf180,tbrf] = dzrootflipmb(n,tb,d1e,d2e,bandsep,nb,rftype,filtAlg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the matched excitation design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rf90 = dzmatchedex(rf180);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate pulse durations and gradient amplitudes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% compute the dwell times for the pulses, subject to peak b1 amplitude
dt180 = max(abs(rf180))/(gamma*maxb1); % rf180 is in radians
if alignedecho
    dt90 = dt180; % 90 has twice length of 180, and echoes coincide,
                  % but different bands have different TE's
else
    dt90 = max(abs(rf90))/(gamma*maxb1); % use min-duration 90
end

% calculate pulse durations
T180 = dt180*n;
T90 = dt90*n*2;
fprintf('Duration of 180: %0.2f ms. Duration of 90: %0.2f ms.\n',T180*1000,T90*1000);

% calculate slice bandwidths
bw180 = tbrf/T180;
bw90 = 2*tbrf/T90;

% calculate gradient strengths
g180 = bw180/(gamma/2/pi*slthick/10); % gauss/cm
g90 = bw90/(gamma/2/pi*slthick/10);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate the combined 90-180 sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x = -10:0.01:10; % cm, range of spatial positions to simulate

% simulate excitation pulse
[a90,b90] = blochsim(rf90,gamma*dt90*g90*ones(2*n,1), x');
% simulate refocusing pulse
[~,b180] = blochsim(rf180,gamma*dt180*g180*ones(n,1), x');

% 90 rewinder phase
gphs = gamma*dt90*g90*(2*n)/2*x';

% calculate Mxy immediately after the 180
Mxy = 2*a90.*conj(b90).*conj(exp(1i*gphs)).*b180.^2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display the pulses and profile
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure; 
t90 = T90*1000*(-n:n-1)/(2*n);
t180 = T180*1000*(-n/2:n/2-1)/n;

% display the 90 pulse
subplot(311)
plot(t90,abs(rf90)/(dt90*gamma));
axis([-T180*1000/2 T180*1000/2 0 maxb1*1.1]);
ylabel 'Gauss'
xlabel 'milliseconds'
title(sprintf('|RF|, 90, duration %0.2f ms',T90*1000));

% display the 180 pulse
subplot(312)
plot(t180,abs(rf180)/(dt180*gamma));
axis([-T180*1000/2 T180*1000/2 0 maxb1*1.1]);
ylabel 'Gauss'
xlabel 'milliseconds'
title(sprintf('|RF|, 180, duration %0.2f ms',T180*1000));

% display the final Mxy amplitude
subplot(313)
plot(x,abs(Mxy));
xlabel 'centimeters'; ylabel '|Mxy| (/M_0)';
title 'Refocused Magnetization Profile'

