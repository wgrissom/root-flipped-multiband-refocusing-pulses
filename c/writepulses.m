% assume that tb, rfall and rf90all are defined

fname = ['mbtb' num2str(tb*10) '.h'];
fp = fopen(fname,'w');

fprintf(fp,['#ifndef MBTB' num2str(tb*10) '_H\n']);
fprintf(fp,['#define MBTB' num2str(tb*10) '_H\n\n']);

fprintf(fp,['#define NT ' num2str(size(rfall,1)) '\n\n']);

% write 180 pulses
for ii = 1:size(rfall,2)
    
    % write real variable
    fprintf(fp,['static int real_mbtb' num2str(tb*10) '_180_' num2str(ii) '[] = \n']);
    fprintf(fp,'{\n');
    
    for jj = 1:size(rfall,1)-1
        
        iout = round(real(rfall(jj,ii))/max(abs(rfall(:,ii)))*32767);
        fprintf(fp,'%i,',iout);
        if rem(jj,10) == 0
            fprintf(fp,'\n');
        end
        
    end
    iout = round(real(rfall(end,ii))/max(abs(rfall(:,ii)))*32767);
    fprintf(fp,'%i\n',iout);
    
    fprintf(fp,'};\n\n');
    
    % write imag variable
    fprintf(fp,['static int imag_mbtb' num2str(tb*10) '_180_' num2str(ii) '[] = \n']);
    fprintf(fp,'{\n');
    
    for jj = 1:size(rfall,1)-1
        
        iout = round(imag(rfall(jj,ii))/max(abs(rfall(:,ii)))*32767);
        fprintf(fp,'%i,',iout);
        if rem(jj,10) == 0
            fprintf(fp,'\n');
        end
        
    end
    iout = round(imag(rfall(end,ii))/max(abs(rfall(:,ii)))*32767);
    fprintf(fp,'%i\n',iout);
    
    fprintf(fp,'};\n\n');
    
end

% write 90 pulses
for ii = 1:size(rf90all,2)
    
    % write real variable
    fprintf(fp,['static int real_mbtb' num2str(tb*10) '_90_' num2str(ii) '[] = \n']);
    fprintf(fp,'{\n');
    
    for jj = 1:size(rf90all,1)-1
        
        iout = round(real(rf90all(jj,ii))/max(abs(rf90all(:,ii)))*32767);
        fprintf(fp,'%i,',iout);
        if rem(jj,10) == 0
            fprintf(fp,'\n');
        end
        
    end
    iout = round(real(rf90all(end,ii))/max(abs(rf90all(:,ii)))*32767);
    fprintf(fp,'%i\n',iout);
    
    fprintf(fp,'};\n\n');
    
    % write imag variable
    fprintf(fp,['static int imag_mbtb' num2str(tb*10) '_90_' num2str(ii) '[] = \n']);
    fprintf(fp,'{\n');
    
    for jj = 1:size(rf90all,1)-1
        
        iout = round(imag(rf90all(jj,ii))/max(abs(rf90all(:,ii)))*32767);
        fprintf(fp,'%i,',iout);
        if rem(jj,10) == 0
            fprintf(fp,'\n');
        end
        
    end
    iout = round(imag(rf90all(end,ii))/max(abs(rf90all(:,ii)))*32767);
    fprintf(fp,'%i\n',iout);
    
    fprintf(fp,'};\n\n');
    
end

% write out the sets of pulses for each nb
fprintf(fp,['static int sets_mbtb' num2str(tb*10) '[] = \n']);
fprintf(fp,'{\n');

for ii = 1:length(setsall)
    
    for jj = 1:length(setsall{ii})-1
        fprintf(fp,'%i,',setsall{ii}(jj));
    end
    if ii == length(setsall)
        % print last number without a comma
        fprintf(fp,'%i\n',setsall{ii}(end));
    else
        % print last number with a comma
        fprintf(fp,'%i,\n',setsall{ii}(end));
    end
    
end

fprintf(fp,'};\n\n');

% write out the optimal phase shifts for each nb
fprintf(fp,['static double phases_mbtb' num2str(tb*10) '[] = \n']);
fprintf(fp,'{\n');

for ii = 1:length(setsall)
    
    for jj = 1:length(setsall{ii})-1
        fprintf(fp,'%.2f,',phsall{ii}(jj));
    end
    if ii == length(setsall)
        % print last number without a comma
        fprintf(fp,'%.2f\n',phsall{ii}(end));
    else
        % print last number with a comma
        fprintf(fp,'%.2f,\n',phsall{ii}(end));
    end
    
end

fprintf(fp,'};\n\n');

% write out the duration-peak b1 products for each nb
fprintf(fp,['static double b1T_mbtb' num2str(tb*10) '_180[] = \n']);
fprintf(fp,'{\n');

for ii = 1:length(b1t180)-1
  fprintf(fp,'%.4f,',b1t180(ii));
end
% print last number without a comma
fprintf(fp,'%.4f\n',b1t180(end));

fprintf(fp,'};\n\n');

% write out the duration-peak b1 products for each nb - 90
fprintf(fp,['static double b1T_mbtb' num2str(tb*10) '_90[] = \n']);
fprintf(fp,'{\n');

for ii = 1:length(b1t90)-1
  fprintf(fp,'%.4f,',b1t90(ii));
end
% print last number without a comma
fprintf(fp,'%.4f\n',b1t90(end));

fprintf(fp,'};\n\n');

% write out the peak amplitude of each subpulse - 180
fprintf(fp,['static double amp_mbtb' num2str(tb*10) '_180[] = \n']);
fprintf(fp,'{\n');

for ii = 1:size(rfall,2)-1
  fprintf(fp,'%.4f,',max(abs(rfall(:,ii))));
end
% print last number without a comma
fprintf(fp,'%.4f\n',max(abs(rfall(:,end))));

fprintf(fp,'};\n\n');

% write out the peak amplitude of each subpulse - 90
fprintf(fp,['static double amp_mbtb' num2str(tb*10) '_90[] = \n']);
fprintf(fp,'{\n');

for ii = 1:size(rf90all,2)-1
  fprintf(fp,'%.4f,',max(abs(rf90all(:,ii))));
end
% print last number without a comma
fprintf(fp,'%.4f\n',max(abs(rf90all(:,end))));

fprintf(fp,'};\n\n');

fprintf(fp,['#endif /* MBTB' num2str(tb*10) '_H */']);
fclose(fp);

