% Script to design a root-flipped multiband refocusing pulse and
% a matched-phase excitation pulse.
% 
% Implements the algorithm in:
%   A Sharma, M Lustig, and W A Grissom. Root-flipped multiband refocusing
%       pulses. Magn Reson Med 2015. doi: 10.1002/mrm.25629
%
% 2015 Anuj Sharma and Will Grissom, Vanderbilt University

addpath ../

gamma = 2*pi*4258; % 1H gyromagnetic ratio (radians/second/gauss)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = 1024;                % # of time points
tb = 4.5;                 % Time-bandwidth product
slthick = 1;            % mm, slice thickness
bandsep = (10:40)*tb;         % Slice gap is 4 slice thickness
alignedecho = 0;        % 0 = design a minimum-duration 90
d1e = 0.001;             % combined Mxy ripple, passband
d2e = 0.001;             % combined Mxy ripple, stopband
maxb1 = 0.18;           % Gauss, system b1 limit
flip = pi;
gradreverse = true;
symmphs = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Design a linear-phase SE pulse/filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[rf,b] = dzrf(n,tb,'se','ls',d1e,d2e);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get all root-flipped versions of same filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

b = b./max(abs(freqz(b))); % normalized beta coefficients
d1 = d1e/4;
% b = b*sin(flip/2 + atan(d1*2)/2); % scale to target flip angle
r = roots(b); % calculate roots of single-band beta polynomial
% sort the roots by phase angle
[~,idx] = sort(angle(r)); 
r = r(idx);r = r(:);
r = leja_fast(r);

candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tb/n*pi;% ...

ball = [];
rfall = [];
rf90all = [];
for ii = 1:2^sum(candidates)
    
    % convert this ii to binary pattern
    doflip = de2bi(ii-1);
    % add zeros for the rest of the passbands
    doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
    % embed in all-roots vector
    tmp = zeros(n-1,1);
    tmp(candidates) = doflip;
    doflip = tmp;
    % flip those indices
    rt = r(:);
    rt(doflip == 1) = conj(1./rt(doflip == 1));
    % get polynomial coefficients back from flipped roots
    tmp = poly(rt);
    %tmp = interp1(0:1/(N-1):1,tmp(:).',0:1/(nt-1):1,'spline',0);
    % important: normalize before modulating! doesn't work
    % for negative shifted slices otherwise
    tmp = tmp./max(abs(freqz(tmp)));
    % store the interpolated and normalized filter
    ball = [ball tmp(:)];
    % store the rf
    rfall = [rfall col(b2rf(tmp(:)*sin(flip/2 + atan(d1*2)/2)))];
    % get a matched 90
    rf90all = [rf90all col(1i*dzmatchedex(rfall(:,end)))];

end

% tb 4.5 main lobe positions, from left to right:
% 4, 2/3/8/12, 1/6/7/10/11/16 (7/10 are 1/2 amp) (center), 5/9/14/15, 13 
% filters with same amplitude are complex conjugates of each other

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop through band separations, comparing construction strategies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nb = 5;
for ii = 1:length(bandsep)
    
    B = exp(1i*2*pi/n*(-n/2+1/2:n/2-1/2)'*((0:nb-1)-(nb-1)/2)*bandsep(ii));
    B90 = exp(1i*2*pi/n*(-n+1/2:n-1/2)'*((0:nb-1)-(nb-1)/2)*bandsep(ii));
    
    switch tb
        case 4.5
            % uses 4 2 7;4 = flipud(13); 2 = flipud(15); 
            setsall = {[1 1 0 0 0 0],...
                [2 1 -2 0 0 0],...
                [2 3 -3 -2 0 0],...
                [2 3 1 -3 -2 0],...
                [2 3 1 -1 -3 -2]};
            phsall = {[0 0 0 0 0 0],...
                [0 0 0 0 0 0],...
                [0 -2.94 -2.94 0 0 0],...
                [0 0.17 2.53 0.17 0 0],...
                [0 -0.34 1.95 1.95 -0.34 0]};
            b1t180 = [0.114 0.115 0.123 0.132 0.0150];
            b1t90 = [0.0820 0.0553 0.0645 0.0880 0.105];
            if ii == 1
              rfall = rfall(:,[7 4 2]);
              rf90all = rf90all(:,[7 4 2]);
            end
            am_c_trmslist_180 = [0.18 0.27 0.32 0.34 0.32];
            am_c_trmslist_90 = [0.09 0.28 0.28 0.19 0.16];
            switch nb
                case 2
                    % two bands, tb 4.5
                    % strategies: 4/13, 2/15, 3/14, 8/9, 12/5, 7/10
                    % sets = {[4 13], [2 15], [3 14], [8 9], [12 5], [7 7]};
                    % for practicality, I think 7/7 wins
                    sets = {[1 1]};
                case 3
                    % three bands, tb 4.5
                    % sets = {[4 1 13], [2 1 15], [4 7 13], [4 10 13]};
                    % 4/7/13 wins
                    sets = {[2 1 2]};
                case 4
                    % four bands, tb 4.5
                    %sets = {[4 2 15 13],[4 3 14 13],[4 7 10 13]};
                    % 4/2/15/13 wins
                    sets = {[2 3 3 2]};
                case 5
                    % five bands, tb 4.5
                    %sets = {[4 2 7 15 13],[4 2 1 15 13]};
                    % 4/2/7/15/13 wins
                    sets = {[2 3 1 3 2]};
                case 6
                    % six bands, tb 4.5
                    %sets = {[4 2 7 7 15 13],[4 2 7 10 15 13],[4 2 1 1 15 13],[4 2 1 6 15 13]};
                    % 4/2/7/7/15/13 wins
                    sets = {[2 3 1 1 3 2]};
                otherwise
                    error 'set not defined for this nb'
            end
        case 3.5
        case 5.5
        otherwise
            error 'this tb does not have defined sets'
    end    
    
    for jj = 1:length(sets)
        
        rfset = rfall(:,sets{jj});
        rfset(:,floor(size(rfset,2)/2)+1:end) = flipud(rfset(:,floor(size(rfset,2)/2)+1:end));
        rfmb(:,jj) = sum(B.*rfset,2);
        % phase optimization
        phs = zeros(nb,1);
        if ii == -1 && nb > 2
            phs = zeros(nb-1,1);
            phsoptrf_max = Inf;
            for kk = 1:50
                phsi = 2*pi*rand(nb-1,1)-pi;
                tmp = fminsearch(@(x)max(abs((B.*rfset)*exp(1i*[0;x]))),phsi);
                if max(abs((B.*rfset)*exp(1i*[0;tmp]))) < phsoptrf_max
                    phs = tmp;
                    phsoptrf_max = max(abs((B.*rfset)*exp(1i*[0;tmp])));
                end
            end
            if symmphs
                if rem(nb,2)
                    phs = [0;col(phs(1:ceil(length(phs)/2)-1));flipud(col(phs(1:floor(length(phs)/2))));0];
                else
                    phs = [0;col(phs(1:ceil(length(phs)/2)-1));flipud(col(phs(1:ceil(length(phs)/2)-1)));0];
                end
            else
                phs = [0;phs(:)];
            end
        end
        
        %rfmbphs(:,jj) = sum(B.*(rfset*diag(exp(1i*phs))),2);
        rfmbphs(:,jj) = sum(B.*(rfset*diag(exp(1i*phsall{nb-1}(1:nb)))),2);
        %rfmbphs(:,jj) = sum(B.*(rfall(:,sets{jj})*diag(exp(1i*mbphstab(nb)))),2);
        
        % design matched 90
        rfset90 = rf90all(:,sets{jj});
        rfset90(:,floor(size(rfset90,2)/2)+1:end) = flipud(rfset90(:,floor(size(rfset90,2)/2)+1:end));
        %rf90phs(:,jj) = sum(B90.*(rfset90*diag(exp(1i*phs))),2);
        rf90phs(:,jj) = sum(B90.*(rfset90*diag(exp(1i*phsall{nb-1}(1:nb)))),2);
    
    end
    
    
    % simulate refocusing pulse
    x = -12:0.01:12; % cm, range of spatial positions to simulate
    dt180 = max(abs(rfmbphs(:,end)))/(gamma*maxb1); % rf180 is in radians
    T180 = dt180*n;
    % calculate slice bandwidths
    bw180 = tb/T180;
    g180 = bw180/(gamma/2/pi*slthick/10); % gauss/cm
    [~,b180f] = blochsim(rfmbphs(:,end),((-1)^(gradreverse))*gamma*dt180*g180*ones(n,1), x');

    % simulate excitation pulse
    dt90 = max(abs(rf90phs(:,end)))/(gamma*maxb1);
    T90 = dt90*2*n;
    bw90 = 2*tb/T90;
    g90 = bw90/(gamma/2/pi*slthick/10); % g/cm
    [a90f,b90f] = blochsim(rf90phs(:,end),gamma*dt90*g90*ones(2*n,1), x');
        
    % 90 rewinder phase
    gphs = gamma*dt90*g90*(2*(n+2*gradreverse-1/2))/2*x';

    % calculate Mxy immediately after the 180
    Mxy = 2*a90f.*conj(b90f).*conj(exp(1i*gphs)).*b180f.^2;

    figure(1);clf;plot(abs(rfmb));hold on;plot(abs(rfmbphs));
    %hold on;plot(abs(rfmbphs));
    figure(2);clf;plot(x,abs(b180f).^2);hold on
    plot(x,abs(2*a90f.*b90f));
    figure(3);clf;plot(x,real(Mxy));
    hold on;plot(x,imag(Mxy));
    plot(x,abs(Mxy))
    
    % report efficiency in uT*s
    fprintf('180 Efficiency: %f uT*s\n',T180*maxb1*100);
    fprintf('90 Efficiency: %f uT*s\n',T90*maxb1*100);
    
    rfs = rfmbphs(:,end);
    am_c_teff = sum(rfs./max(abs(rfs))*32767)/(32767*length(rfs))
    am_c_trms = sum((abs(rfs)./max(abs(rfs))*32767).^2)/(32767^2*length(rfs))
    am_c_tabs = sum(abs(rfs)./max(abs(rfs))*32767)/(32767*length(rfs))
    am_c_sym = find(abs(rfs) == max(abs(rfs)))/length(rfs)
    
    
    rfs = rf90phs(:,end);
    am_c_teff = sum(rfs./max(abs(rfs))*32767)/(32767*length(rfs))
    am_c_trms = sum((abs(rfs)./max(abs(rfs))*32767).^2)/(32767^2*length(rfs))
    am_c_tabs = sum(abs(rfs)./max(abs(rfs))*32767)/(32767*length(rfs))
    am_c_sym = find(abs(rfs) == max(abs(rfs)))/length(rfs)
    
    pause;
    
end

