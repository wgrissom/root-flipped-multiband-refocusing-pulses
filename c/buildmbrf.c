#include "mbtb45.h"

#include <math.h>

#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286

#define MAXNB 6
#define MINNB 2

void buildmbrf(double *rf90r,double *rf90i,double *rf180r,double *rf180i,int nb,double tb,double bandsep,bool optphs)
{
  /* return matched 90 + 180 pulses for this number 
   * of bands, time-bandwidth product, and integer band 
   * separation. Output pulses are in radians */

  /* Arguments:
   *rf90r,*rf90i: pointers to previously-created length-NT double-precision vectors for the 90 real and imaginary parts (radians)
   *rf180r, *rf180i: same, for the 180 (radians)
   nb: number of bands (between MINNB and MAXNB)
   tb: time-bandwidth product to use (as of 8/10/15, only tb = 4.5 is available)
   bandsep: slice separation in units of slice thicknesses
   optphs: switch to apply optimized inter-band phases (do it always!) */

  /* Also available: the variables b1T_mbtb*_180 and b1T_mbtb*_90 contain the product of the peak b1 and duration (uT*seconds)
     for each nb, which are approximately constant functions of bandsep. This way, you can figure out how long your pulse
     will be for a given peak b1, before building it! */ 
  
  int i,j,k;
  int pulseind;
  double arg,modr,modi;
  
  bandsep *= tb; /* convert to integer */
  
  /* zero out the waveforms */
  for(i = 0;i < NT;i++) { rf180r[i] = 0; rf180i[i] = 0; }
  for(i = 0;i < 2*NT;i++) { rf90r[i] = 0; rf90i[i] = 0; }
  
  if(nb > MAXNB | nb < MINNB) return;

  /* build the 180 */
  for(i = 0;i < NT;i++){
    
    for(j = 0;j < nb;j++){
      
      /* calculate modulation */
      arg = 2*PI/(double)NT*((double)i-(double)NT/2.0+0.5)*((double)j - ((double)nb-1.0)/2.0)*bandsep;
      /* apply optimal inter-band phases if requested */
      if(optphs) arg += phases_mbtb45[(nb-MINNB)*MAXNB+j];
      modr = cos(arg);
      modi = sin(arg);
      
      /* flip time index if doing second half of pulses */
      pulseind = sets_mbtb45[(nb-MINNB)*MAXNB+j];
      if(pulseind < 0) k = -i + (NT - 1); else k = i; 
      
      switch(abs(pulseind)){
	
      case 1:
	rf180r[i] += amp_mbtb45_180[0]*(real_mbtb45_180_1[k]*modr - imag_mbtb45_180_1[k]*modi);
	rf180i[i] += amp_mbtb45_180[0]*(real_mbtb45_180_1[k]*modi + imag_mbtb45_180_1[k]*modr);
	break;
      case 2:
	rf180r[i] += amp_mbtb45_180[1]*(real_mbtb45_180_2[k]*modr - imag_mbtb45_180_2[k]*modi);
	rf180i[i] += amp_mbtb45_180[1]*(real_mbtb45_180_2[k]*modi + imag_mbtb45_180_2[k]*modr);
	break;
      case 3:
	rf180r[i] += amp_mbtb45_180[2]*(real_mbtb45_180_3[k]*modr - imag_mbtb45_180_3[k]*modi);
	rf180i[i] += amp_mbtb45_180[2]*(real_mbtb45_180_3[k]*modi + imag_mbtb45_180_3[k]*modr);
	break;
        
      }
      
    }

  }
  for(i = 0;i < NT;i++){
    rf180r[i] /= (double)32767;
    rf180i[i] /= (double)32767;
  }
  
  /* build the 90 */
  for(i = 0;i < 2*NT;i++){
    
    for(j = 0;j < nb;j++){
      
      /* calculate modulation */
      arg = 2*PI/(double)NT*((double)i-(double)NT+0.5)*((double)j - ((double)nb-1.0)/2.0)*bandsep;
      /* apply optimal inter-band phases if requested */
      if(optphs) arg += phases_mbtb45[(nb-MINNB)*MAXNB+j];
      modr = cos(arg);
      modi = sin(arg);
      
      /* flip time index if doing second half of pulses */
      pulseind = sets_mbtb45[(nb-MINNB)*MAXNB+j];
      if(pulseind < 0) k = -i + (2*NT - 1); else k = i; 

      switch(abs(pulseind)){
	
      case 1:
	rf90r[i] += amp_mbtb45_90[0]*(real_mbtb45_90_1[k]*modr - imag_mbtb45_90_1[k]*modi);
	rf90i[i] += amp_mbtb45_90[0]*(real_mbtb45_90_1[k]*modi + imag_mbtb45_90_1[k]*modr);
	break;
      case 2:
	rf90r[i] += amp_mbtb45_90[1]*(real_mbtb45_90_2[k]*modr - imag_mbtb45_90_2[k]*modi);
	rf90i[i] += amp_mbtb45_90[1]*(real_mbtb45_90_2[k]*modi + imag_mbtb45_90_2[k]*modr);
	break;
      case 3:
	rf90r[i] += amp_mbtb45_90[2]*(real_mbtb45_90_3[k]*modr - imag_mbtb45_90_3[k]*modi);
	rf90i[i] += amp_mbtb45_90[2]*(real_mbtb45_90_3[k]*modi + imag_mbtb45_90_3[k]*modr);
	break;
        
      }
      
      
    }

  }
  for(i = 0;i < 2*NT;i++){
    rf90r[i] /= (double)32767;
    rf90i[i] /= (double)32767;
  }
  
}

double calc_mb_am_c_trms(double *rfr,double *rfi,int n){

  double am_c_trms;
  int i;
  double mag2,maxmag2;
  
  maxmag2 = 0.0;
  am_c_trms = 0.0;
    
  for(i = 0;i < n;i++){
    mag2 = rfr[i]*rfr[i] + rfi[i]*rfi[i];
    am_c_trms += mag2;
    if(mag2 > maxmag2) maxmag2 = mag2;
  }

  am_c_trms /= maxmag2*(double)n;

  return am_c_trms;
  
}

double calc_mb_am_c_teff(double *rfr,double *rfi,int n,int flip){

  /* assumes input pulse is in radians */
  
  int i;
  double mag2,maxmag2;
  double am_c_teff;
  
  maxmag2 = 0.0;
  
  for(i = 0;i < n;i++){
    mag2 = rfr[i]*rfr[i] + rfi[i]*rfi[i];
    if(mag2 > maxmag2) maxmag2 = mag2;
  }

  am_c_teff = (PI*(double)flip/180.0)/((double)n*sqrt(maxmag2));
  
  return am_c_teff;
  
}
