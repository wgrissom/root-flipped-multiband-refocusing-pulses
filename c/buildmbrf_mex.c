#include "mex.h"
#include "mbtb45.h"

#include "buildmbrf.c"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    double *rf90r, *rf90i, *rf180r, *rf180i;
    double am_c_trms_90, am_c_teff_90, am_c_trms_180, am_c_teff_180;
    int nb;
    double tb, bandsep;
    bool optphs;
    
    if ((nrhs != 4) || (nlhs != 2))
        mexErrMsgTxt("Usage: [rf90,rf180] = buildmbrf_mex (nb,tb,bandsep,optphs)");
    
    nb = (int)mxGetScalar(prhs[0]);
    tb = mxGetScalar(prhs[1]);
    bandsep = mxGetScalar(prhs[2]);
    optphs = (bool) mxGetScalar(prhs[3]);
    
    plhs[0] = mxCreateDoubleMatrix (1, 2*NT, mxCOMPLEX);
    rf90r = mxGetPr(plhs[0]);rf90i = mxGetPi(plhs[0]);
    plhs[1] = mxCreateDoubleMatrix (1, NT, mxCOMPLEX);
    rf180r = mxGetPr(plhs[1]);rf180i = mxGetPi(plhs[1]);
    
    /* get alpha polynomial */
    buildmbrf (rf90r, rf90i, rf180r, rf180i, nb, tb, bandsep, optphs);

    am_c_trms_90 = calc_mb_am_c_trms(rf90r,rf90i,2*NT);
    am_c_teff_90 = calc_mb_am_c_teff(rf90r,rf90i,2*NT,90);
    printf("90: am_c_trms = %f, am_c_teff = %f\n",am_c_trms_90,am_c_teff_90);

    am_c_trms_180 = calc_mb_am_c_trms(rf180r,rf180i,NT);
    am_c_teff_180 = calc_mb_am_c_teff(rf180r,rf180i,NT,180);
    printf("180: am_c_trms = %f, am_c_teff = %f\n",am_c_trms_180,am_c_teff_180);

    return;
    
}


